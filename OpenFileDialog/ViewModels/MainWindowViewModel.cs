using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using OpenFileDialog.Commands;
using OpenFileDialog.Infrastructure;

namespace OpenFileDialog.ViewModels
{
    public sealed class MainWindowViewModel : INotifyPropertyChanged
    {
        string _filePath, _fileContent;

        public MainWindowViewModel()
        {
            //normally the GetFilePathOpenDialogService I've hardcoded would be injected into ctor through
            //an external mechanism with possible alternate implementation passed in during unit test
            OpenFileDialogCommand = new GetFileCommand(new GetFilePathOpenDialogService());
        }

        public ICommand OpenFileDialogCommand { get; }

        public string FilePath
        {
            get => _filePath;
            set
            {
                if (value == _filePath) return;
                _filePath = value;
                OnPropertyChanged();
            }
        }

        public string FileContent
        {
            get => _fileContent;
            internal set
            {
                if (value == _fileContent) return;
                _fileContent = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}