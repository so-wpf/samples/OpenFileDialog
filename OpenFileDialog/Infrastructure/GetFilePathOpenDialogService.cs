namespace OpenFileDialog.Infrastructure
{
    public sealed class GetFilePathOpenDialogService : IGetFilePathService
    {
        public string GetFilePath(string folder)
        {
            var dialog = new Microsoft.Win32.OpenFileDialog { InitialDirectory = folder };

            return dialog.ShowDialog() == true ? dialog.FileName : null;
        }
    }
}