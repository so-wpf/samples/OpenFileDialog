namespace OpenFileDialog.Infrastructure
{
    public interface IGetFilePathService
    {
        string GetFilePath(string folder);
    }
}