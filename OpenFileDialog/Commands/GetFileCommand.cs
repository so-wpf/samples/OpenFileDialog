using System;
using System.IO;
using System.Windows.Input;
using OpenFileDialog.Infrastructure;
using OpenFileDialog.ViewModels;

namespace OpenFileDialog.Commands
{
    public sealed class GetFileCommand : ICommand
    {
        readonly IGetFilePathService _getFilePathService;
        public GetFileCommand(IGetFilePathService getFilePathService) => _getFilePathService = getFilePathService;

        //Since we'll have a dialog window open, there's no need to disable the button
        //We are dependent on properties provided by MainWindowViewModel so just make sure we
        //have that. In production you'd want to abstract this property access away behind an
        //interface or perhaps a dedicated viewmodel used to extract information from command
        public bool CanExecute(object parameter) => parameter is MainWindowViewModel;

        public void Execute(object parameter)
        {
            var vm = (MainWindowViewModel) parameter;

            try
            {
                var basePath = string.IsNullOrWhiteSpace(vm.FilePath) || !File.Exists(vm.FilePath)
                    ? Environment.CurrentDirectory
                    : Path.GetDirectoryName(vm.FilePath);

                vm.FilePath = _getFilePathService.GetFilePath(basePath);

                if (string.IsNullOrWhiteSpace(vm.FilePath) || !File.Exists(vm.FilePath)) return;

                using(var stream =new FileStream(vm.FilePath, FileMode.Open, FileAccess.Read))
                using (var reader = new StreamReader(stream))
                    vm.FileContent = reader.ReadToEnd();

            }
            catch (Exception ex)
            {   //Sample quality error handling
                //obviously you're not supposed to handle exceptions like this in production!
                vm.FileContent = ex.Message;
            }
        }

        //Sample quality event raising. In production you should be listening to properties
        //that CanExecute relies on and raising CanExecuteChanged as PropertyChanged is raised
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }
}